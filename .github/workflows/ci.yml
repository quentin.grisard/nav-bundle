name: CI

on:
  schedule:
    - cron: 0 2 * * 1-5
  pull_request:
  push:
    branches:
      - master

jobs:
  tests:
    name: Tests PHP ${{ matrix.php }} ${{ matrix.dependency }} (Symfony ${{ matrix.symfony }})
    runs-on: ubuntu-latest
    strategy:
      # https://github.community/t5/GitHub-Actions/GitHub-Actions-Matrix-options-dont-work-as-documented/m-p/29558
      matrix:
        php:
          - '7.2'
          - '7.3'
          - '7.4'
        symfony:
          - '4.4.*'
          - '5.0.*'
        include:
          - php: '7.4'
            coverage: true
        dependency:
          - ''
          - 'lowest'
      fail-fast: false
    steps:
      - name: Checkout
        uses: actions/checkout@v1

      - name: Setup PHP
        uses: shivammathur/setup-php@v1
        with:
          php-version: ${{ matrix.php }}
          extensions: curl, dom, iconv, intl, json, libxml, mbstring, phar, soap, tokenizer, xdebug, xml, xmlwriter

      - name: Get Composer Cache Directory
        id: composer-cache
        run: echo "::set-output name=dir::$(composer config cache-files-dir)"

      - name: Cache dependencies
        uses: actions/cache@v1
        with:
          path: ${{ steps.composer-cache.outputs.dir }}
          key: ${{ runner.os }}-composer-${{ hashFiles('**/composer.json') }}
          restore-keys: ${{ runner.os }}-composer-

      - name: Configure Symfony
        run: |
          composer global require symfony/flex
          composer config extra.symfony.require "${{ matrix.symfony }}"

      - name: Update project dependencies
        if: matrix.dependency == ''
        run: composer update --no-progress --no-suggest --ansi --prefer-stable

      - name: Update project dependencies lowest
        if: matrix.dependency == 'lowest'
        run: composer update --no-progress --no-suggest --ansi --prefer-lowest --prefer-stable

      - name: Run php-cs-fixer tests
        run: vendor/bin/php-cs-fixer fix --dry-run --diff

      - name: Run behat tests
        run: vendor/bin/behat

      - name: Run phpstan tests
        if: matrix.dependency == ''
        run: vendor/bin/phpstan analyze

      - name: Run phpunit tests
        run: vendor/bin/phpunit --coverage-clover clover.xml

      - name: Upload report to Coveralls
        env:
          COVERALLS_REPO_TOKEN: ${{secrets.GITHUB_TOKEN}}
        if: matrix.coverage && matrix.dependency == ''
        run: |
          composer global require --dev --prefer-dist --no-progress --no-suggest --ansi --prefer-stable cedx/coveralls
          $HOME/.composer/vendor/bin/coveralls clover.xml

      - name: Bundle is bootable
        if: matrix.php == '7.4'
        env:
          SKELETON_VERSION: ${{matrix.symfony}}
        run: |
          composer create-project "symfony/skeleton:${SKELETON_VERSION}" flex
          cd flex
          composer config extra.symfony.allow-contrib true
          composer req --ignore-platform-reqs vincentchalamon/nav-bundle
